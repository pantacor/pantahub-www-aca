import { makeStyles } from '@material-ui/core/styles'

const primaryColor = 'rgb(66,66,98)'
const successColor = 'rgb(140,194,72)'
const warningColor = 'rgb(255,150,6)'
const dangerColor = 'rgb(220,65,47)'
const disabledColor = 'rgb(189,189,189)'

const backupFontFamily = ['Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', 'sans-serif']
const mediumFontFamily = ['SFPTMedium', ...backupFontFamily]

export const useStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(8)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%' // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  linearProgress: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2)
    }
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1)
    }
  },
  vertialSpace: {
    marginBottom: theme.spacing(2)
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6)
  },
  button: {
    'text-transform': 'capitalize !important',
    'font-family': mediumFontFamily,
    'font-size': '15px !important',
    'line-height': '1.5 !important',
    'box-shadow': 'initial !important'
  },
  primary: {
    backgroundColor: `${primaryColor} !important`,
    color: 'white !important'
  },
  success: {
    backgroundColor: `${successColor} !important`,
    color: 'white !important'
  },
  warning: {
    backgroundColor: `${warningColor} !important`,
    color: 'white !important'
  },
  danger: {
    backgroundColor: `${dangerColor} !important`,
    color: 'white !important'
  },
  disabled: {
    backgroundColor: `${disabledColor} !important`,
    color: 'white !important'
  },
  colorDisabled: {
    opacity: 0.3
  },
  successOption: {
    backgroundColor: `${successColor} !important`,
    color: 'white !important'
  },
  warningOption: {
    backgroundColor: `${warningColor} !important`,
    color: 'white !important'
  },
  dangerOption: {
    backgroundColor: `${dangerColor} !important`,
    color: 'white !important'
  },
  notSelected: {
    opacity: 0.3,
    '&:hover': {
      opacity: 1.0
    }
  },
  selected: {
    opacity: 1.0
  },
  fullWidth: {
    width: '100%'
  }
}))
