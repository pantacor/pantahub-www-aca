import { useRef, useEffect, useCallback } from 'react'

const REFRESH_RATE = Number(process.env.REACT_APP_REFRESH_RATE || '4000')

export function useInterval (callback, autostart = true, delay = REFRESH_RATE) {
  const savedCallback = useRef(null)
  const intervalId = useRef(undefined)
  const polling = useRef(0)

  const tick = () => {
    if (savedCallback.current) {
      savedCallback.current(polling.current)
      polling.current++
    }
  }

  const removeInterval = () => {
    clearInterval(intervalId.current)
    intervalId.current = undefined
  }

  const startInterval = useCallback(() => {
    if (intervalId.current && delay === null) {
      removeInterval()
    }

    if (delay !== null) {
      if (!intervalId.current) {
        intervalId.current = setInterval(tick, delay)
        tick()
      }
      return () => {
        removeInterval()
      }
    }
  }, [delay])

  useEffect(() => {
    savedCallback.current = callback
  })

  useEffect(() => {
    if (autostart) {
      tick()
    }
  }, [autostart])

  useEffect(() => {
    if (autostart) {
      return startInterval()
    }
  }, [autostart, startInterval])

  return {
    startInterval,
    removeInterval,
    tick
  }
}
