import React from 'react'
import { createMuiTheme } from '@material-ui/core/styles'
import ThemeProvider from '@material-ui/styles/ThemeProvider'
import SFPTMedium from '../assets/fonts/SFProText-Medium.ttf'
import SFPTSemibold from '../assets/fonts/SFProText-Semibold.ttf'

const backupFontFamily = ['Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', 'sans-serif']
const mediumFontFamily = ['SFPTMedium', ...backupFontFamily]

const sfptMedium = {
  fontFamily: 'SFPTMedium',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400,
  src: `
    local('SFPTMedium'),
    url(${SFPTMedium})
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF'
}

const sfptSemibold = {
  fontFamily: 'SFPTSemibold',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400,
  src: `
    local('SFPTSemibold'),
    url(${SFPTSemibold})
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF'
}

export const theme = createMuiTheme({
  typography: {
    fontFamily: mediumFontFamily
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [sfptMedium, sfptSemibold]
      }
    }
  },
  palette: {
    primary: {
      main: '#424262',
      contrastText: '#fff'
    },
    secondary: {
      main: 'rgb(140,194,72)',
      contrastText: '#fff'
    }
  },
  status: {
    danger: 'orange'
  },
  shadows: ['none']
})

export const ThemeProviderLoaded = ({ children }) => (
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>
)
