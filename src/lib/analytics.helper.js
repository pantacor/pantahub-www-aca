
const defaultPayload = { category: 'click' }

export function Track (event, p = {}) {
  const payload = {
    ...defaultPayload,
    ...p
  }

  if (process.env.REACT_APP_DEBUG) {
    console.info(event, payload)
  }

  if (window.gtag) {
    window.gtag('event', event, payload)
  }
}
