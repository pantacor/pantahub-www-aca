import React from 'react'
import { Provider } from 'react-redux'

import CreateStore from './store'
import IsAuthenticated from './components/organisms/IsAuthenticated/IsAuthenticated'
import './App.scss'

const store = CreateStore()

function App () {
  return (
    <Provider store={store}>
      <IsAuthenticated/>
    </Provider>
  )
}

export default App
