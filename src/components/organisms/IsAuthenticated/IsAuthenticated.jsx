/* eslint-disable no-restricted-globals */
import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Login, GetUser, SaveTokenAndRedirect } from '../../../store/auth/actions'
import { STATUS } from '../../../store/auth/reducer'
import MainLayout from '../../layouts/Main'
import Home from '../../pages/Home'
import Welcome from '../../pages/Welcome'

function IsAuthenticated ({ children, status, token, Login, GetUser, SaveTokenAndRedirect }) {
  const onSignInClick = () => {
    Login()
  }

  useEffect(() => {
    const hash = window.location.hash

    if (hash !== '') {
      SaveTokenAndRedirect(hash)
    }
  }, [token, SaveTokenAndRedirect, Login])

  useEffect(() => {
    if (status === null) {
      GetUser()
    }
  }, [GetUser, status])

  if (status === STATUS.INPROGRESS) {
    return (
      <CircularProgress />
    )
  }
  const hash = window.location.hash

  return (
    <div className='App'>
      <MainLayout status={status}>
        {(!hash && !token
          ? <Welcome onSignInClick={onSignInClick}/>
          : <Home />)}
      </MainLayout>
    </div>
  )
}

export default connect(
  (state) => ({
    token: state.auth.token,
    status: state.auth.status
  }),
  {
    Login,
    GetUser,
    SaveTokenAndRedirect
  }
)(IsAuthenticated)
