import React from 'react'
import { HowToFlash } from '../molecules/HowFlash/HowToFlash'

export default function Home () {
  return (
    <HowToFlash />
  )
}