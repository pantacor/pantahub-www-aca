/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React from 'react'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

export const useStyles = makeStyles(theme => ({
  logo: {
    padding: theme.spacing(5)
  },
  signInButton: {
    width: '219px',
    height: '35px',
    'border-radius': '3px',
    'background-color': '#414262',
    'margin-top': theme.spacing(2),
    'margin-botton': theme.spacing(2),
    padding: theme.spacing(0, 0, 0, 0),
    '&:hover': {
      'background-color': '#414262',
      opacity: 0.7
    }
  },
  signInText: {
    'font-family': 'SFPTMedium',
    'font-weight': 500,
    'text-transform': 'initial',
    'font-size': '15px',
    'letter-spacing': '0px',
    'line-height': '16px',
    color: '#ffffff',
    padding: theme.spacing(0, 0.5, 0, 0)
  }
}))

export default function Welcome (props) {
  const classes = useStyles()

  return (
    <Grid
      container
      align="center"
      justify="center"
      alignContent="center"
      className="welcome"
    >
      <Button onClick={props.onSignInClick} className={classes.signInButton}>
        <span className={classes.signInText}>Sign in with</span> <img src={process.env.PUBLIC_URL + '/pantacor.png'} alt='Pantacor' height='25px' />
      </Button>
    </Grid>
  )
}
