import React from 'react'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import Link from '@material-ui/core/Link'
import Container from '@material-ui/core/Container'
import LogoutButton from '../molecules/LogoutButton/LogoutButton'
import { ThemeProviderLoaded } from '../../lib/theme.helper'
import { useStyles } from '../../hooks/useStyles'

import logo from '../../assets/images/pantacor-brand.png'

export function Copyright () {
  return (
    <Typography variant='body2' color='textSecondary' align='center'>
      {'1.1.1.1 for Families is a trademark of Cloudflare, Inc'}
      <br />
      {'Copyright © '}
      <Link color='inherit' href='https://www.pantahub.com'>
        Pantacor
      </Link>{' '}
      {new Date().getFullYear()}
    </Typography>
  )
}

export default function MainLayout ({ status, children }) {
  const classes = useStyles()

  return (
    <ThemeProviderLoaded>
      <CssBaseline />
      <LogoutButton />
      <header>
        <Container maxWidth='md'>
          <img
            style={{ margin: '32px 0px 0px 0px' }}
            src={logo}
            alt='Pantahub ACA-TPM'
            height='60px'
          />
        </Container>
      </header>
      <main>
        {children}
      </main>
      <footer className={classes.footer}>
        <a
          className="twitter-share-button"
          data-size="large"
          href="https://twitter.com/intent/tweet?text=I just turned my Raspberry Pi into a Wi-fi Hotspot with:">
          Tweet
        </a>
        <br/><br/>
        <Copyright />
      </footer>
    </ThemeProviderLoaded>
  )
}
