import React from 'react'
import { Track } from '../../../lib/analytics.helper'

export function Tracker ({ event, payload = {}, children }) {
  const trackOnClick = (e) => {
    Track(event, payload)
  }

  return (
    <div onClick={trackOnClick}>
      {children}
    </div>
  )
}
