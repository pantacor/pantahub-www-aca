import React from 'react'
import Grid from '@material-ui/core/Grid'
import { Done, Warning } from '@material-ui/icons'
import Loading from '../../atoms/Loading/Loading'
import { CURRENT_STEP_NOTIFICATIONS } from '../../../lib/const'

export default function NotificationIcon ({ classes, expanded, status }) {
  return (
    <Grid item>
      {!expanded &&
        <React.Fragment>
          {status === CURRENT_STEP_NOTIFICATIONS.LOADING &&
            <Loading
              divClassName={`${classes.deviceHeaderLoadingWrapper}`}
              imgClassName={`${classes.deviceHeaderLoading}`} />}
          {status === CURRENT_STEP_NOTIFICATIONS.SUCCESS &&
            <Done fontSize='small' className={`${classes.deviceSuccessNotification}`} />}
          {status === CURRENT_STEP_NOTIFICATIONS.WARNING &&
            <Warning fontSize='small' className={`${classes.deviceWarningNotification}`} />}
        </React.Fragment>
      }
    </Grid>
  )
}
