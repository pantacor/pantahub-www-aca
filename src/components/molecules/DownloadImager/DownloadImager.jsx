import React from 'react'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Grow from '@material-ui/core/Grow'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import { ThemeProviderLoaded } from '../../../lib/theme.helper'
import { Track } from '../../../lib/analytics.helper'

const options = [
  {
    name: 'Select your OS'
  },
  {
    name: 'Download for Windows',
    link: 'https://downloads.raspberrypi.org/imager/imager.exe'
  },
  {
    name: 'Download for Mac OS',
    link: 'https://downloads.raspberrypi.org/imager/imager.dmg'
  },
  {
    name: 'Download for Linux',
    link: 'https://downloads.raspberrypi.org/imager/imager_amd64.deb'
  }
]

function getOS () {
  const userAgent = navigator.userAgent.toLowerCase()
  if (userAgent.indexOf('win') >= 0) return 'windows'
  if (userAgent.indexOf('mac') >= 0) return 'macos'
  if (userAgent.indexOf('linux') >= 0) return 'linux'
  return 'Unknown OS'
}

function defaultSelected () {
  switch (getOS()) {
    case 'windows':
      return 1
    case 'macos':
      return 2
    case 'linux':
      return 3
    default:
      return 0
  }
}

export default function DownloadImager () {
  const [open, setOpen] = React.useState(false)
  const anchorRef = React.useRef(null)
  const [selectedIndex, setSelectedIndex] = React.useState(defaultSelected())

  const handleClick = () => {
    console.info(`You clicked ${options[selectedIndex]}`)
    if (options[selectedIndex].link) {
      Track('download-imager', { value: options[selectedIndex].name })
      window.open(options[selectedIndex].link)
    } else {
      handleToggle()
    }
  }

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index)
    setOpen(false)
    window.open(options[index].link)
  }

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen)
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }

    setOpen(false)
  }

  return (
    <Grid container direction="column" alignItems="center">
      <Grid item xs={12}>
        <ThemeProviderLoaded>
          <ButtonGroup variant="contained" color="secondary" ref={anchorRef} aria-label="split button">
            <Button onClick={handleClick}>{options[selectedIndex].name}</Button>
            <Button
              color="secondary"
              size="small"
              aria-controls={open ? 'split-button-menu' : undefined}
              aria-expanded={open ? 'true' : undefined}
              aria-label="select merge strategy"
              aria-haspopup="menu"
              onClick={handleToggle}
            >
              <ArrowDropDownIcon />
            </Button>
          </ButtonGroup>
        </ThemeProviderLoaded>
        <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'
              }}
            >
              <Paper>
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList id="split-button-menu">
                    {options.map((option, index) => (
                      <MenuItem
                        key={option.name}
                        disabled={index === 0}
                        selected={index === selectedIndex}
                        onClick={(event) => handleMenuItemClick(event, index)}
                      >
                        {option.name}
                      </MenuItem>
                    ))}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </Grid>
    </Grid>
  )
}
