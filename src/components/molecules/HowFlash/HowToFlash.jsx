import React, { useState } from 'react'
// import Button from '@material-ui/core/Button'
import Container from '@material-ui/core/Container'
import Grow from '@material-ui/core/Grow'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'

import ImageButton from '../ImageButton/ImageButton'
import DownloadImager from '../DownloadImager/DownloadImager'

import InstallGIF from '../../../assets/images/install.gif'
import './howtoflash.scss'
import { ThemeProviderLoaded } from '../../../lib/theme.helper'
import { Tracker } from '../../atoms/Tracker/Tracker'

export function HowToFlash () {
  const [showMore] = useState(true)

  // const OnShowMore = () => {
  //   setShowMore(!showMore)
  //   window.scrollTo(0, 0)
  // }

  return (
    <Container className="how-to-flash">
      <br/>
      <Grow
        in={showMore}>
        <div style={{ display: !showMore ? 'none' : 'block' }}>
          <Grid
            container
            spacing={2}
            direction="row"
            justify="center"
            alignItems="center"
            className="content">
            <Grid
              container
              justify="center"
              alignItems="center"
              className="content"
            >
              <Grid item sm={6}>
                <header className="mt--4 mb--4">
                  <Typography variant="h3" component="h1">
                    What is and how works an ACA-TPM attestation flow?
                  </Typography>
                </header>
              </Grid>
            </Grid>
            <Grid item sm={4}>
              <section className="intro--content">
                <Typography variant="body1" >
                  Pantahub presents the new way to onboard devices using your Trusted Plataform Module (TPM)
                  to securily onboard and connect to Pantahub.
                </Typography>
                <Typography variant="body1" >
                  This allow your device to register and authenticate your device using TLS backed by your TPM
                  in that the certificate used will be only usable for you computer.
                </Typography>
                <ol>
                  <li>The device registers with the ACA to get a IAK certificate</li>
                  <li>Using the IAK the device register on pantahub and recive a iDevID from Pantahub CA</li>
                  <li>
                    The IDevID certificate is the one used in conjuction
                    with a key in the TPM to create a TLS connection with Pantahub API
                  </li>
                </ol>
              </section>
            </Grid>
            <Grid item sm={6} >
              <img className="max-width-img" src="/images/ACA-TPM-flow.png" alt="ACA-TPM flow description" />
            </Grid>
          </Grid>
          <section className="step">
            <header className="mt--4 mb--4">
              <Typography variant="h3" component="h1">
                How to use it with Pantahub
              </Typography>
            </header>
            <header className="d-flex d-flex-row align-items-center">
              <div>
                <section className="number">
                  1
                </section>
                <section className="title">
                  <h1>
                    Download Pantavisor installer
                  </h1>
                  <p>
                    Download our pantavisor installer in order to create your instalation media, normaly
                    you will use a USB pendrive as instalation media
                  </p>
                </section>
              </div>
            </header>
            <Grid
              container
              spacing={2}
              direction="row"
              justify="center"
              alignItems="center"
              className="content">
              <Grid item>
                <Tracker event="download-image" payload={{ category: 'click', value: 'rpi4' }}>
                  <ImageButton device="x64-uefi"/>
                </Tracker>
              </Grid>
            </Grid>
          </section>
          <section className="step">
            <header className="d-flex d-flex-row align-items-center">
              <section className="number">
                2
              </section>
              <section className="title">
                <h1>
                  Download and install Raspberry Pi Imager
                </h1>
              </section>
            </header>
            <section className="content">
              <p>
                Imager is used to write the OS image you downloaded in Step 1 into your instalation media.
              </p>
              <DownloadImager />
            </section>
          </section>
          <section className="step">
            <header className="d-flex d-flex-row align-items-center">
              <section className="number">
                3
              </section>
              <section className="title">
                <h1>
                  Flash Pantavisor image into your USB drive
                </h1>
              </section>
            </header>
            <section className="content">
              <p>
                1. Launch Raspberry Pi Imager
                <br/>
                2. Choose the file you downloaded in Step 1
                <br/>
                3. Select your USB media and click Flash
                <br/><br/>
                <b>WARNING: This will wipe all data on this card</b>
              </p>
              <br/>
              <p>
                <img src={InstallGIF} alt="How to flash using Raspberry Pi imager animation" />
              </p>
              <p><b>Note:</b> The flashing process could take a while</p>
            </section>
          </section>
          <section className="step">
            <header className="d-flex d-flex-row align-items-center">
              <section className="number">
                4
              </section>
              <section className="title">
                <h1>
                  Install using your installer media
                </h1>
              </section>
            </header>
            <section className="content">
              <p>
                Select the Pantavisor installer (auto) method.
                This will do the factory process for the device,
                it will create required keys used by the attestion process.
              </p>
              <video
                controls
                autoPlay
                loop
                muted
                width="1024">
                <source
                  src="/videos/tpm-aca-flow-720.webm"
                  type="video/webm"
                />
                <source
                  src="/videos/tpm-aca-flow-720.m4v"
                  type="video/mp4"
                />
                {`Sorry, your browser doesn't support embedded videos.`}
              </video>
              <p>
                When this process is finished your new device will appear on your Pantahub dashboard you can start
                doing having fun doing some new trails.
              </p>
              <p>
                Happy Trailing!
              </p>
            </section>
          </section>
        </div>
      </Grow>
      <ThemeProviderLoaded>
        <Tracker
          event='toggle-how-to'
          payload={{ category: 'click', value: !showMore ? ' Setup a fresh Raspberry Pi ' : 'Hide Guide' }}
        >
          {/* <Button
            variant="contained"
            color="primary"
            size="medium"
            onClick={OnShowMore} >
            { !showMore ? ' Setup a fresh Raspberry Pi ' : 'Hide Guide' }
          </Button> */}
        </Tracker>
      </ThemeProviderLoaded>
    </Container>
  )
}
