import React from 'react'
import { connect } from 'react-redux'
import Button from '@material-ui/core/Button'
import * as Service from '../../../services/devices.service'
import { ThemeProviderLoaded } from '../../../lib/theme.helper'

const IMAGER_URL = 'https://images.apps.pantahub.com/get'
const DEFAULT_CHANNEL = '&channel=release-candidate'
const INSTALLER_QUERY = '&variant=installer'

export const host = new URL(process.env.REACT_APP_PH_API_URL || 'https://api.pantahub.com').hostname

export function ImageButton ({ token, device }) {
  if (token === null) {
    return null
  }

  device = device.toLowerCase()
  const deviceArch = device.replace('-installer', '')

  const Download = async () => {
    const response = await Service.getFactoryToken(token)
    if (!response.ok) {
      throw Error(`Can't get device auto join token`)
    }
    const config = window.btoa('autojointoken.aca.json=' + JSON.stringify(response.json) + '\0')
    const url = IMAGER_URL + '?device=' + device + '&config=' + config + DEFAULT_CHANNEL + INSTALLER_QUERY
    console.info(url)
    window.open(url)
  }

  return (
    <ThemeProviderLoaded>
      <Button
        variant="contained"
        color="secondary"
        size="medium"
        onClick={Download}
      >
        Download {deviceArch} Image
      </Button>
    </ThemeProviderLoaded>
  )
}

export default connect(state => ({
  token: state.auth.token
}))(ImageButton)
