import { combineReducers } from 'redux'

import errors from './general-errors/reducer'
import auth from './auth/reducer'

const rootReducer = () => combineReducers({
  errors,
  auth
})

export default rootReducer
