import {
  _getJSON,
  _postJSON,
  _putJSON,
  _patchJSON,
  API_URL
} from '../lib/api.helper'

const DEVS_URL = `${API_URL}/devices`
const SUMMARY_URL = `${API_URL}/trails/summary`

const devicesUrl = () =>
  `${DEVS_URL}/`

const deviceUrl = (deviceId) =>
  `${DEVS_URL}/${deviceId}`

const devicesTokenUrl = () =>
  `${DEVS_URL}/tokens`

const trailsUrl = (deviceId) =>
  `${API_URL}/trails/${deviceId}/steps`

const trailStepRevisionUrl = (deviceId, rev) =>
  `${API_URL}/trails/${deviceId}/steps/${rev}`

const deviceTrailsSummaryUrl = (id) =>
  `${API_URL}/trails/${id}/summary`

export const getDeviceTrailsSummary = async (token, id) =>
  _getJSON(deviceTrailsSummaryUrl(id), token)

export const getAllTrailsSummaries = async (token) =>
  _getJSON(SUMMARY_URL, token)

export const postDeviceTrailStep = async (token, id, payload) =>
  _postJSON(trailsUrl(id), token, payload)

export const getFactoryToken = async (token) =>
  _postJSON(devicesTokenUrl(), token, {})

export const setDeviceMetadata = async (token, id, meta, type = 'user-meta') =>
  _putJSON(`${DEVS_URL}/${id}/${type}`, token, meta)

export const getAllDevices = async (token) =>
  _getJSON(devicesUrl(), token)

export const getDevice = async (token, id) =>
  _getJSON(deviceUrl(id), token)

export const getDeviceTrails = async (token, id) =>
  _getJSON(trailsUrl(id) + '?progress.status={%22$ne%22:%22%22}', token)

export const getDeviceTrailStep = async (token, id, rev) =>
  _getJSON(trailStepRevisionUrl(id, rev), token)

export const patchDevice = async (token, id, payload) =>
  _patchJSON(`${DEVS_URL}/${id}`, token, payload)
