FROM node:lts as builder

ARG NS=production

WORKDIR /builder

COPY . .

ENV NODE_EVN=production

RUN set -a \
    && REACT_APP_REVISION=$(git --work-tree=../ describe --always --tags)-$NS \
    && . /builder/env.$NS \
    && set +a && yarn && yarn build

FROM nginx

COPY --from=builder /builder/build /usr/share/nginx/html

RUN sed -i "/index.*index.html.*/ a \
\        try_files \$uri /index.html;" /etc/nginx/conf.d/default.conf
