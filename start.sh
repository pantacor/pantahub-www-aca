#!/bin/bash

if [ ! -f .env.local ]; then
  cp env.local .env.local
fi

if [ ! -f .env.development ]; then
  cp env.development .env.development
fi

if [ ! -f .env.production ]; then
  cp env.production .env.production
fi

yarn react-scripts start
